package com.lcg.datafabric.customer.retail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.service.ExtractionService;

import ch.qos.logback.classic.Level;

@RestController
public class RetailExtractionController {

	@Autowired
    private DataFabricLogger2 dataFabricLogger;
	
	@Autowired
	private ExtractionService extractionService;
	
	@RequestMapping("/startrextraction")
	public String startExtractionManaully() {
		dataFabricLogger.log(Level.INFO, RetailExtractionController.class, 
                "Retail extraction endpoint called, starting extraction manually", 
                	Level.INFO.levelStr);
		try {
			extractionService.startExtraction();
		}catch(Exception ex) {
			dataFabricLogger.log(Level.ERROR, RetailExtractionController.class, 
        			"Error during extracting retail data manually", Level.ERROR.levelStr, ex);
		}
		return "Retail extraction process started. Check application logs for progress.\n";
	}
	
}
