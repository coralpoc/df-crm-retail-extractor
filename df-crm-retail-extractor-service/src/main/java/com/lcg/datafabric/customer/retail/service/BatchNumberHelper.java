package com.lcg.datafabric.customer.retail.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class BatchNumberHelper {

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd_HHmm");
    public static final String BATCH_PREFIX = "RETAIL";
   

    public static String getBatchNumber(LocalDateTime lDate) {
    	String dateString = DATE_FORMAT.format(lDate);
        return BATCH_PREFIX + "_" + dateString;
    }
}
