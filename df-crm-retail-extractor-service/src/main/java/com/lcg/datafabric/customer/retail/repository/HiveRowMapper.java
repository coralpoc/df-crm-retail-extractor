package com.lcg.datafabric.customer.retail.repository;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.RowMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HiveRowMapper implements RowMapper<Map<String,Object>>{

	@Override
	public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		Map<String, Object> mapped = new HashMap<String, Object>();
		ResultSetMetaData resultMetadata = rs.getMetaData();
		int size = resultMetadata.getColumnCount();
		for(int i=1;i <= size; i++) {
			mapped.put(resultMetadata.getColumnName(i),rs.getObject(i));
		}
		
		log.debug("Mapped data for row - {}",rowNum);
		return mapped;
	}

}
