package com.lcg.datafabric.customer.retail;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.service.ExtractionService;
import com.lcg.datafabric.customer.retail.service.StreamerService;

import ch.qos.logback.classic.Level;

@SpringBootApplication
@EnableScheduling
public class RetailExtractorApplication implements ApplicationRunner{
	
	@Value("${spring.application.name}")
	private String appName;

	public static void main(String[] args) {
		SpringApplication.run(RetailExtractorApplication.class, args);
	}
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		dataFabricLogger().log(Level.INFO, "Application started at " + DateTime.now(),Level.INFO.levelStr);
		
	}
	
	@Bean
	public ExtractionService extractorService() {
		return new ExtractionService();
	}
	
	@Bean
	public DataFabricLogger2 dataFabricLogger() {
		
		return new DataFabricLogger2(appName);
	}
	

	
}
