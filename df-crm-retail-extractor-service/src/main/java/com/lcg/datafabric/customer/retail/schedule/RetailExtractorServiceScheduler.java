package com.lcg.datafabric.customer.retail.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.service.DynamoDBStatusService;
import com.lcg.datafabric.customer.retail.service.ExtractionService;

import ch.qos.logback.classic.Level;

@Component
public class RetailExtractorServiceScheduler {
	
	@Autowired
    private DataFabricLogger2 dataFabricLogger;
	
	@Autowired
	private ExtractionService extractionService;
	
	@Autowired
	private DynamoDBStatusService dynamoDBStatusService;
    
	@Scheduled(cron="${retail.extraction.cron}")
	public void initiateExtractionService() {
		try {
			
			if(dynamoDBStatusService.getDplExtractionStatus()) {
				dataFabricLogger.log(Level.INFO, RetailExtractorServiceScheduler.class, 
		                "Retail Extraction scheduler triggered", Level.INFO.levelStr);
				extractionService.startExtraction();
			}
		}catch(Exception ex) {
			dataFabricLogger.log(Level.ERROR, RetailExtractorServiceScheduler.class, 
            			"Error during extracting retail data", Level.ERROR.levelStr, ex);
		}
	}
}
