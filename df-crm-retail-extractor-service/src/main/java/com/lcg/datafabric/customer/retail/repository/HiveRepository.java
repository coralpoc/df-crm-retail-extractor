package com.lcg.datafabric.customer.retail.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.retail.dto.ControlTableResponse;

import ch.qos.logback.classic.Level;
import lombok.extern.slf4j.Slf4j;

@Repository
@Transactional(readOnly=true)
@Slf4j
public class HiveRepository {

	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private DataFabricLogger2 dataFabricLogger;

	private final String COLUMN_NAMES = "username,brand,customeridentifier,retailshopcardmgmntsegmnt,featurespacescore,featurespacereason,retailnearestshop,retailfirstfobtbetdate,retaillastfobtbetdate,retailfirstotcbetdate,retaillastotcbetdate,retailshopcardnumber,retailcustomertype,retailacctvrflag,retailmobileverified,retailemailverified,retailcurrentvip,retailharmminimisationscore,retailharmminimisationearlywar,retailpreferredshop,retailpreferredshopname,retaillastshopplayedin,retaillastshopplayedinname,retailhasbetinshopcard,retailhasbetfobt,retailhasbetotc,retailhasbetssbt,retailfobtmastersegmentflag,retaillifecyclesegmentflag,retaillastfobtbetdateover50amt,retaillastroulettefobtbetdate,retaillastslotsfobtbetdate,retaillatstfobtotherbetdate,retailfobttotalstakeamt,retailfobtplayerdays,retailfobttotalstakeover50amt,retailfobtlast7daysstakeamt,retailfobtlast7daysplayerdays,retailfobtlast28daysstakeamt,retailfobtlast28daysplayerdays,retailfobttotalbonusamtredeeme,retailfobtbonusbankamtredeemed,retaillastfootballotcbetdate,retaillasthorseracingotcbetdat,retaillastotcotherbetdate,retailotctotalstakeamt,retailotcplayerdays,retailotclast7daysstakeamt,retailotclast7daysplayerdays,retailotclast28daysstakeamt,retailotclast28daysplayerdays,retailotctotalbonusamtredeemed,retailfirstssbtbetdate,retaillatestssbtbetdate,retailssbttotalstakeamt,retailssbtplayerdays,retaillatestvipbetdate,retailviptotalstakes,retailviptotalplayerdays,retailviptotalstakesover50,retailviplast28daystakes,retailviplast28dayplayerdays,retailiviplast28daystakesovr50,retailbonusabuserfobt,retailbonusabuserotc,retailfallowcell,retailfobtcrmstatus,retailotccrmstatus,retailpreferredshopcountry,retailsegmentationstatus,retailchannelprimarypref,retailprimarysegment,retailfobtprimarysegment,retailfobtsegmentstatus,retailfobttgwbanding,retailfobtproductpref,retailfobtstakingtypebanding,retailfobttenure,retailsportsprimarysegment,retailsportssegmentstatus,retailsportstgwbanding,retailsportsproductpref,retailsportstenure,retailssbtlast7daysstakeamt,retailssbtlast7daysplayerdays,retailssbtlast28daysstakeamt,retailssbtlast28daysplayerdays,load_date";
	
	public int getTotalCount() {
		return jdbcTemplate.queryForObject("select count(*) from lcg_internal_db.salesforce_staging",Integer.class);
	}
	
	public List<Map<String,Object>> fetchAllRetailDataInBatches(int startRow, int pageSize){
		
		try {
			String query = String.format("select %s from lcg_internal_db.salesforce_staging LIMIT %d, %d", COLUMN_NAMES, startRow, pageSize);
			return jdbcTemplate.query(query, new HiveRowMapper());
		}catch(DataAccessException de) {
			dataFabricLogger.log(Level.ERROR, HiveRepository.class, "An error occurred while querying hive.", "ERROR", de);
		}
		return null;
	}
	
	public Optional<ControlTableResponse> queryControlTableForAvailableData(final LocalDate runDate){
		
		
		String query = "select etl_date,record_count from LC_HADOOP_PROCESS_MONITOR where project = 'RetailSF' and date(etl_date) = ?";
		
		dataFabricLogger.log(Level.DEBUG, HiveRepository.class, "Control table SQL query - " + query, "DEBUG");
		
		try {
			return jdbcTemplate.execute(query, new PreparedStatementCallback<Optional<ControlTableResponse>>() {

				@Override
				public Optional<ControlTableResponse> doInPreparedStatement(PreparedStatement ps)
						throws SQLException, DataAccessException {
					
					ps.setObject(1, runDate.format(DateTimeFormatter.ISO_LOCAL_DATE));
					
					ResultSet rs = ps.executeQuery();
					
					if(rs != null) {
						boolean populated = false;
						ControlTableResponse resp = new ControlTableResponse();
						int count = 0;
						while(rs.next()) {
							resp.setEtlDateTime(rs.getTimestamp(1).toLocalDateTime());
							resp.setRecordCount(rs.getInt(2));
							populated = true;
							count++;
						}
						rs.close();
						
						//This is a failsafe incase multiple row exist for a particular run date
						if(count > 1) {
							String msg = String.format("Extracted %s rows for date %s in control table, should be at most one entry", count,runDate);
							dataFabricLogger.log(Level.WARN, HiveRepository.class, msg, "WARN");
						}
						
						if(populated) {
							return Optional.of(resp);
						}
					}

					return Optional.empty();
				}
				
			});
			
		}catch(DataAccessException de) {
			dataFabricLogger.log(Level.ERROR, HiveRepository.class, "An error occurred while querying hive.", "ERROR", de);
			return Optional.empty();
		}
	}
	
}
