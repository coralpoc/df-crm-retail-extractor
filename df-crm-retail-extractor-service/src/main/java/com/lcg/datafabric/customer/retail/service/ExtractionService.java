package com.lcg.datafabric.customer.retail.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lc.df.logging.DataFabricLogger2;
import com.lcg.datafabric.customer.dpl.extractor.view.status.ViewStatus;
import com.lcg.datafabric.customer.retail.dto.ControlTableResponse;
import com.lcg.datafabric.customer.retail.repository.HiveRepository;

import ch.qos.logback.classic.Level;

public class ExtractionService {
	
	private LocalDate todayRun;
	
	@Value("${spring.application.name}")
	private String appName;

	@Value("${hive.fetch.page-size}")
	private int fetchPageSize;

	@Value("${extractor.streamer.enabled}")
	private boolean streamerEnabled;


	@Autowired
	private HiveRepository hiveRepository;
	
	@Autowired
	private DataFabricLogger2 dataFabricLogger;
	
	@Autowired
	private DynamoDBStatusService dynamoService;
	
	@Autowired
	private StreamerService streamerservice;
	
	@Autowired
	private ELKReportService eLKReportService;
	
	public void setStreamerService(StreamerService streamerservice) {
		this.streamerservice = streamerservice;
	}
	
	public void setDynamoDBStatusService(DynamoDBStatusService dynamoService) {
		this.dynamoService = dynamoService;
	}
	
	public void setDataFabricLogger(DataFabricLogger2 dataFabricLogger) {
		this.dataFabricLogger = dataFabricLogger;
	}
	
	public void setHiveRepository(HiveRepository hiveRepository) {
		this.hiveRepository = hiveRepository;
	}
	
	public void setStreamerEnabled(Boolean streamerEnabled) {
		this.streamerEnabled = streamerEnabled;
	}
	
	public boolean isStreamerEnabled() {
		return streamerEnabled;
	}
	
	public void startExtraction() {
		
		try {
		
			todayRun = LocalDate.now();
			String msg = String.format("Started extraction process at %s", LocalDateTime.now());
			dataFabricLogger.log(Level.INFO, ExtractionService.class, msg, "INFO" );
			
			msg = String.format("Checking control table for data availability for date - %s", todayRun);
			dataFabricLogger.log(Level.DEBUG, ExtractionService.class, msg, "DEBUG" );
			
			LocalDateTime runDate = LocalDateTime.now();
			String batchNo = BatchNumberHelper.getBatchNumber(runDate);
			
			Optional<ControlTableResponse> optionResponse = hiveRepository.queryControlTableForAvailableData(todayRun);
			
			if(optionResponse.isPresent()){
				
				ControlTableResponse contResponse = optionResponse.get();
				
				dataFabricLogger.log(Level.INFO, ExtractionService.class, "Found load status record in hive, for etlDate " + contResponse.etlDateTime + " with record count - " + contResponse.getRecordCount()  , "INFO" );

				eLKReportService.sendExtractionPerViewInfoToELK(DynamoDBStatusService.RETAIL_VIEW_NAME, contResponse);
				
				dynamoService.creatRetailEntryForDate(runDate, contResponse.getEtlDateTime(),batchNo);
				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Created a new entry in dynamoDB for retail", "DEBUG" );

				
				dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTING);
				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTING", "DEBUG" );
				
				
				
				if (isStreamerEnabled()) {
					eLKReportService.sendExtractorInfoToELK( appName, batchNo, contResponse.getRecordCount(), "Retail extracts will be processed by service.");
					extractAndProcessInBatches(batchNo, contResponse);
				} else {
					eLKReportService.sendExtractorInfoToELK( appName, batchNo, contResponse.getRecordCount(), "Retail extracts will be sent to a kafka topic.");
					extractInBatchesAndSendToTempKafka(batchNo, contResponse);
					
				}
			}else {

				dataFabricLogger.log(Level.INFO, ExtractionService.class, "Retail data not yet available in hive", "INFO");
			}
		
		}catch(Exception e) {
			
			dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTION_FAILED);
			
			dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTION_FAILED", "DEBUG" );
			
			dataFabricLogger.log(Level.ERROR, ExtractionService.class, "An unexpceted error occurred during extraction flow.", "ERROR", e);
		}
	}

	private void extractInBatchesAndSendToTempKafka(String batchNo, ControlTableResponse contResponse) {
		String msg;
		int startFromRow = 0;
		int fetchingBatchNo = 1;
		int processedRows = 0;
		int totalCount = hiveRepository.getTotalCount();
		dataFabricLogger.log(Level.INFO, ExtractionService.class, String.format("Total number of records to be extracted [%d]", totalCount), "INFO");
		for (int i = 0; i < totalCount; i = i + fetchPageSize) {
			List<Map<String, Object>> dataList = hiveRepository.fetchAllRetailDataInBatches(startFromRow, fetchPageSize);

			if(isValidExtractedData(dataList)) {

				eLKReportService.sendExtractorInfoToELK(appName, batchNo, dataList.size(), "Extracted this many rows from BI Hive for RETAIL in batch[" + fetchingBatchNo + "]");

				msg = String.format("Retail Data available for date - %s, extracted %d out of %d", contResponse.etlDateTime, fetchingBatchNo * fetchPageSize, totalCount);
				dataFabricLogger.log(Level.INFO, ExtractionService.class, msg, "INFO");

				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Started streaming extracted records to temp kafka topic.", "DEBUG" );
				streamerservice.sendToTempKafkaTopic(batchNo, fetchingBatchNo, dataList);
				processedRows++;

				dataFabricLogger.log(Level.INFO, ExtractionService.class, "Retail extraction flow completed for batch[" + fetchingBatchNo + "]", "INFO");
				fetchingBatchNo++;
			}else {
				eLKReportService.sendExtractorInfoToELK(appName, batchNo, 0, "An execption occurred while extracting from BI Hive for RETAIL in batch[" + fetchingBatchNo + "], rangeFrom - " + startFromRow);
				dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTION_FAILED);
				dataFabricLogger.log(Level.INFO, ExtractionService.class, "Connection error at Hive, marked status as EXTRACTION_FAILED", "INFO");
				break;
			}
			
			startFromRow = startFromRow + fetchPageSize;
		}

		String finalProcessMsg = String.format("Processed [%d] rows out of [%d]", processedRows, contResponse.getRecordCount());
		dataFabricLogger.log(Level.INFO, ExtractionService.class, finalProcessMsg, "INFO");

		dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTED);
		dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTED", "DEBUG" );
	}

	private void extractAndProcessInBatches(String batchNo, ControlTableResponse contResponse) {
		boolean extractionSuccess = true;
		String msg;
		int startFromRow = 0;
		int fetchingBatchNo = 1;
		int processedRows = 0;
		int totalCount = hiveRepository.getTotalCount();
		dataFabricLogger.log(Level.INFO, ExtractionService.class, String.format("Total number of records to be extracted [%d]", totalCount), "INFO");
		for (int i = 0; i < totalCount; i = i + fetchPageSize) {
			List<Map<String, Object>> dataList = hiveRepository.fetchAllRetailDataInBatches(startFromRow, fetchPageSize);

			if(isValidExtractedData(dataList)) {

				eLKReportService.sendExtractorInfoToELK(appName, batchNo, dataList.size(), "Extracted this many rows from BI Hive for RETAIL in batch[" + fetchingBatchNo + "]");

				msg = String.format("Retail Data available for date - %s, extracted %d out of %d", contResponse.etlDateTime, fetchingBatchNo * fetchPageSize, totalCount);
				dataFabricLogger.log(Level.INFO, ExtractionService.class, msg, "INFO");

				dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Started streaming extracted records to kafka from ", "DEBUG" );
				streamerservice.processExtracts(batchNo, fetchingBatchNo, dataList);
				processedRows++;


				dataFabricLogger.log(Level.INFO, ExtractionService.class, "Retail extraction flow completed for batch[" + fetchingBatchNo + "]", "INFO");
				fetchingBatchNo++;
			}else {
				eLKReportService.sendExtractorInfoToELK(appName, batchNo, 0, "An execption occurred while extracting from BI Hive for RETAIL in batch[" + fetchingBatchNo + "], rangeFrom - " + startFromRow);
				dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTION_FAILED);
				String hiveErrorMsg = String.format("Connection error at Hive, marked status as EXTRACTION_FAILED. Failed on fetching batch no [%d]", fetchingBatchNo);
				dataFabricLogger.log(Level.INFO, ExtractionService.class, hiveErrorMsg, "INFO");
				extractionSuccess = false;
				break;
			}
			
			startFromRow = startFromRow + fetchPageSize;
		}

		String finalProcessMsg = String.format("Processed [%d] rows out of [%d]", processedRows, contResponse.getRecordCount());
		dataFabricLogger.log(Level.INFO, ExtractionService.class, finalProcessMsg, "INFO");

		if (extractionSuccess) {
			dynamoService.updateRetailRunStatus(todayRun, ViewStatus.EXTRACTED);
			dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to EXTRACTED", "DEBUG");

			dynamoService.updateRetailRunStatus(todayRun, ViewStatus.SENT_TO_KAFKA);
			dataFabricLogger.log(Level.DEBUG, ExtractionService.class, "Changed retail view status in dynamoDB to SENT_TO_KAFKA", "DEBUG");
		}
	}


	private boolean isValidExtractedData(List<Map<String, Object>> dataList) {
		
		boolean flag = true;
		
		if(dataList == null) {
			flag = false;
		}else if(dataList.isEmpty()) {
			dataFabricLogger.log(Level.INFO, ExtractionService.class, "This should not happen, no record extracted from hive", "INFO");
			flag = false;
		}
		
		return flag;
	}


}
