package com.lcg.datafabric.customer.retail.dto;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class ControlTableResponse {
	
	public LocalDateTime etlDateTime;
	public int recordCount;

}
