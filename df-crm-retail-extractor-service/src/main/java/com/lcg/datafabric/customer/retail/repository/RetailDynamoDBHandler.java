package com.lcg.datafabric.customer.retail.repository;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.UpdateItemRequest;
import com.lcg.datafabric.customer.dpl.extractor.dto.ExtractionStatusKey;
import com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.DynamoDBHandler;
import com.lcg.datafabric.customer.dpl.extractor.view.status.ViewStatus;

import lombok.extern.slf4j.Slf4j;

import static com.lcg.datafabric.customer.dpl.extractor.view.dynamodb.ExtractionStatusTableColumns.*;

@Slf4j
public class RetailDynamoDBHandler extends DynamoDBHandler {
	
	public static final String EXTRACTION_STATUS_TABLE_NAME = "retail-extraction-status";

	public RetailDynamoDBHandler(AmazonDynamoDB dynamoDB) {
		super(dynamoDB);
	}
	
	public UpdateItemRequest getUpdateItemRequest(ExtractionStatusKey statusKey, ViewStatus viewStatus) {
        AttributeValue currentTimeAttributeValue = buildCurrentTimeAttributeValue();
        log.info("Updating view {} to status {}", statusKey.getTableName(), viewStatus);
        Map<String, AttributeValue> attributeValues = new HashMap<>();
        attributeValues.put(":extractionStatus", new AttributeValue().withS(viewStatus.name()));

        String timeStampColumnName = null;
        String updateExpression = null;
        switch(viewStatus) {
            case EXTRACTING: {
                timeStampColumnName = extractionStartTime.name();
                updateExpression = buildUpdateExpression("extractionStatus", timeStampColumnName);
                attributeValues.put(":" + timeStampColumnName, currentTimeAttributeValue);
                log.info("Setting {} to {}", timeStampColumnName, currentTimeAttributeValue.getS());
                break;
            }
            case EXTRACTED: {
                timeStampColumnName = extractionEndTime.name();
                updateExpression = buildUpdateExpression("extractionStatus", timeStampColumnName);
                attributeValues.put(":" + timeStampColumnName, currentTimeAttributeValue);
                log.info("Setting {} to {}", timeStampColumnName, currentTimeAttributeValue.getS());
                break;
            }
            case SENT_TO_KAFKA: {
                timeStampColumnName = "streamingEndTime";
                updateExpression = buildUpdateExpression("extractionStatus", timeStampColumnName);
                attributeValues.put(":" + timeStampColumnName, currentTimeAttributeValue);
                log.info("Setting {} to {}", timeStampColumnName, currentTimeAttributeValue.getS());
                break;
            }
            default: {
                updateExpression = "SET extractionStatus = :extractionStatus";
            }
        }

        UpdateItemRequest updateItemRequest = new UpdateItemRequest()
                .withTableName(EXTRACTION_STATUS_TABLE_NAME)
                .withKey(
                        new AbstractMap.SimpleEntry("sourceAvailDate", new AttributeValue()
                                .withS(statusKey.getSourceAvailabilityDate().format(DateTimeFormatter.ISO_LOCAL_DATE))),
                        new AbstractMap.SimpleEntry("tableName", new AttributeValue()
                                .withS(statusKey.getTableName()))
                )
                .withUpdateExpression(updateExpression)
                .withExpressionAttributeValues(attributeValues);
        return updateItemRequest;
    }
	
	private AttributeValue buildCurrentTimeAttributeValue() {
        Instant currentTime = Instant.now();
        return new AttributeValue().withS(currentTime.atZone(ZoneOffset.UTC).format(DateTimeFormatter.ISO_DATE_TIME));
    }

}
