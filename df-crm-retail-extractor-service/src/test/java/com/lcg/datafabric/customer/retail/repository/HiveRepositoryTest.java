package com.lcg.datafabric.customer.retail.repository;

import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.lcg.datafabric.customer.retail.dto.ControlTableResponse;

import junit.framework.Assert;

@IfProfileValue(name = "spring.profiles.active", value = "local")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HiveRepositoryTest {
	
	@Autowired
	private HiveRepository hiveRepo;
	
	
	@Test
	public void testRepositoryConfig() {
		int count = hiveRepo.getTotalCount();
		assertTrue(count > 0);
	}
	
	@Test
	public void testReadFromControlTable() {
		
		LocalDate lDate = LocalDate.now();
		Optional<ControlTableResponse> optResponse = hiveRepo.queryControlTableForAvailableData(lDate);
		
		assertTrue(optResponse.isPresent());
	}
}
