package com.lcg.datafabric.customer.retail.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import com.zaxxer.hikari.HikariDataSource;

@IfProfileValue(name = "spring.profiles.active", value = "local")
@RunWith(SpringJUnit4ClassRunner.class)
public class HiveConnectionTest {
	
	
	private static JdbcTemplate jdbcTemplate;
	

	@BeforeClass
	public static void initJdbc() {
		jdbcTemplate = new JdbcTemplate(dataSource());
	}
	
	private static DataSource dataSource() {
		return DataSourceBuilder.create().type(HikariDataSource.class)
				.driverClassName("com.cloudera.hive.jdbc41.HS2Driver")
				.url("jdbc:hive2://localhost:10000/lcg_internal_db;AuthMech=3;")
				.username("lcgcrm")
				.password("lcgcrm265").build();
	} 

	
	@Test
	public void countRowsInTable() {
        int count = JdbcTestUtils.countRowsInTable(this.jdbcTemplate, "salesforce_staging");
        assertTrue(count > 0);
    }

	@Test
	public void testConnection() {
		int count = jdbcTemplate.queryForObject("select count(*) from lcg_internal_db.salesforce_staging",Integer.class);
		assertTrue(count > 0);
	}
	
	@Test
	public void testJdbcQuery() {
		List<Map<String,Object>> objList = jdbcTemplate.query("select top 10 * from lcg_internal_db.salesforce_staging", new HiveRowMapper());
		assertEquals(objList.size(), 10);
	}
	

}
